# Script to analyze the nucleotide composition at the 5’end, position 10 and 3’end

This script uses an annotated bed file to determine the nucleotide composition. 

## Used tools:

fastx-toolkit/0.0.13

bedtools/2.25.0

R/2.15.3

python/2.7.3

weblogo/3.4

Used R packages:

scales_0.2.3

matrixStats_0.8.5
 
reshape2_1.2.2

reshape_0.8.4

plyr_1.8

cba_0.2-12

## Input:

As input file a collapsed bed.gz file was used.

The bed.gz is a tab seperated file which looks as follows:

> chr2L   11316489        11316515        HWI-ST1253F-0259:8:1113:14526:16858#26166_TACGAGCCGGTGACATTGCTCGGGTG?TE_AS@u~8  8       -

Col1: Chromosome

Col2: start

Col3: end

Col4: Identifier – composed of –Sequencing identifier _ sequence of read ? annotated category @ unique or multimapper ~ read count

Col5: read counts

Col6: plus or minus strand of mapping

Multimappers will have multiple entries in the bed file.

## Variables which need to be set: 

**NAMEforANALYSIS**= The name for files and output genrated

**rawFOLDER**= Folder which contains raw files which can be used for further analyses 

**rawTMP**=Temporary files which can be deleted afterwards

**rawOUTPUT**= Folder for output fiels

**CUTOFFPPM**= PPM cutoff according to library mapping as describe in Materials and Methods. Eg.: 10.36

**NSLOTS**= CPUs available for the analysis

**pINPUT**=path for input files used in this analysis

**INPU**T= specify the exact input file eg: INPUT="${pINPUT}${NAMEforANALYSIS}_annotated.bed.gz"

## Output

The tool will generate .pdf files (showing bits and probability) as well as .txt files containing the probability for nucleotide composition at 5'end positon 10 and 3'end. The binning analysis will generate NAME\_for\_ANALYSIS\_a\_nucleotide\_counts.txt to NAME\_for\_ANALYSIS\_j\_nucleotide_counts.txt which contain the nucleotide counts at the 3'end+1 position for each bin. Further processing needs to be done in EXCEL. NAME\_for\_ANALYSIS\_average\_def.txt contains the average definition for each bin.

