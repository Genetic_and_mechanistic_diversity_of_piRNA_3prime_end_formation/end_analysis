
module load fastx-toolkit/0.0.13
module load bedtools/2.25.0
module load R/2.15.3
module load python/2.7.3
module load weblogo/3.4




set -u

#-------------------------------------------------------------------------------------------------------
#presetting of variables 
NAMEforANALYSIS=
#Folder of files to keep - use for other analysis
rawFOLDER=
#temp folder
rawTMP=
#output folder
rawOUTPUT=

#ppm cutoff as defined in materials and methods
CUTOFFPPM=

#Cores avaiable for computing
NSLOTS=10

#path to folder were input files are stored
pINPUT=

#-------------------------------------------------------------------------------------------------------
#prepare all vairables
FOLDER="${rawFOLDER}${NAMEforANALYSIS}/${NAMEforANALYSIS}"
TMP="${rawTMP}${NAMEforANALYSIS}/${NAMEforANALYSIS}"
#specify the exact input file eg: INPUT="${pINPUT}${NAMEforANALYSIS}_annotated.bed.gz"
INPUT=
#specify were the fiel with analyzable 5'ends are stored - generated by script also published
ANALYZABLE="/groups/brennecke/handler/results/analyzable_5ends/dm3_2016-01-11-35mer/analyzable_regions.bed"
OUTPUT="${rawOUTPUT}${NAMEforANALYSIS}/${NAMEforANALYSIS}"
#-------------------------------------------------------------------------------------------------------
#create all folders
mkdir -p "${rawFOLDER}${NAMEforANALYSIS}"
mkdir -p "${rawTMP}${NAMEforANALYSIS}"
mkdir -p "${rawOUTPUT}${NAMEforANALYSIS}"

echo "start"
echo ${NAMEforANALYSIS}
echo ${CUTOFFPPM}

#extract unique and multimappers annotated to the TE category
    zcat ${INPUT} |\
    awk ' ($4~"@u|@m") && ($4~"TE@|TE_AS@")  {print}' |\
#split identifier for later collapsing into single entry
    awk '{OFS="\t"; split($4,i,"_"); print $0,i[1]}' > ${TMP}_curated.bed 
#collapse multiple entries into single entry 
    sort --parallel=$NSLOTS -k7,7  ${TMP}_curated.bed | awk -F"\t" '!a[$7]++' > ${FOLDER}_curated_collapsed.bed
    cat ${FOLDER}_curated_collapsed.bed | awk -v NAME=${TMP} ' {OFS="\t"; if($6=="+") print $1,$2,$2+1,$4,$5,$6 > NAME"_unique_5end_plus.bed"; else print $1,$3-1,$3,$4,$5,$6 > NAME"_unique_5end_minus.bed" }'
#intersect with valid 5' ends
    bedtools intersect -s -wa -a  ${TMP}_unique_5end_plus.bed -b ${ANALYZABLE} | gzip > ${TMP}_unique_5end_plus_valid.bed.gz
    bedtools intersect -s -wa -a ${TMP}_unique_5end_minus.bed -b ${ANALYZABLE} | gzip > ${TMP}_unique_5end_minus_valid.bed.gz
#extract location of 5'end, counts, length of sequenced read
    zcat ${TMP}_unique_5end_plus_valid.bed.gz ${TMP}_unique_5end_minus_valid.bed.gz | awk '{OFS="\t"; split($4,a,"_"); split(a[2],b,"?"); print $0,b[1],length(b[1])}' > ${TMP}_unique_5end_all_valid_split
    sort --parallel=$NSLOTS -k1,1 -k2,2n ${TMP}_unique_5end_all_valid_split| awk '{OFS="\t"; print $1"!"$6"@"$2,$8,$5}' | awk ' {OFS="\t"; if($2<=35) print }' > ${FOLDER}_length_table_all
#Run R-script to generate tables 
      echo "starting R script:"
      echo $NAMEforANALYSIS
      echo $CUTOFFPPM
    Rscript /groups/brennecke/JS_RH_3end/git/end_analysis/length_clustering-IP_28Apr.R NAME=${NAMEforANALYSIS} CUTOFFPPM=${CUTOFFPPM} TMP=${rawFOLDER} 
#-------------------------------------------------------------------------------------------------------
#make compund 3end logo
#generate a bedfile from the length clustering output centered around the dominant 3'end of piRNAs, generate a 5bp window around this position
 cat ${FOLDER}_length-filtered.txt |  sed '1d' | grep -v "sep" | awk  -v NAME=${TMP} '  {OFS="\t"; split($1,a,"!"); split(a[2],b,"@"); if(b[1]=="+") print a[1],b[2]+$3-5,b[2]+$3+5,$1,$2,b[1] > NAME"_3end_of_piRNA_plus.bed"; else print a[1],b[2]+1-($3)-5,b[2]+1-($3)+5,$1,$2,b[1] > NAME"_3end_of_piRNA_minus.bed" }'

#extract sequences from positions defined - get fasta was used to extract sequqnces
 cat ${TMP}_3end_of_piRNA_plus.bed | sort -k1,1 -k2,2n | bedtools getfasta -fi /groups/brennecke/mohn/indexes/dm3_r5_45/dmel-all-chromosome-r5.45.modif.for.index.wo.uextra.fasta -bed stdin -fo ${TMP}_3end_of_piRNA_plus.fasta
 #revers complement for minus strand is generated to generate a compound sequence logo 
 cat ${TMP}_3end_of_piRNA_minus.bed | sort -k1,1 -k2,2n | bedtools getfasta -fi /groups/brennecke/mohn/indexes/dm3_r5_45/dmel-all-chromosome-r5.45.modif.for.index.wo.uextra.fasta -bed stdin -fo ${TMP}_3end_of_piRNA_minus.fasta
 fastx_reverse_complement -i ${TMP}_3end_of_piRNA_minus.fasta -o ${TMP}_3end_of_piRNA_minus_rc.fasta
 cat ${TMP}_3end_of_piRNA_plus.fasta ${TMP}_3end_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U bits -A rna -F pdf -n 50 -c classic -s large -t ${NAMEforANALYSIS} -o ${OUTPUT}_3end_of_piRNA_length_logo.pdf
 cat ${TMP}_3end_of_piRNA_plus.fasta ${TMP}_3end_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U probability -A rna -F pdf -n 50 -c classic -o ${OUTPUT}_3end_of_piRNA_length_logo_prob.pdf
 cat ${TMP}_3end_of_piRNA_plus.fasta ${TMP}_3end_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U probability -A rna -F logodata -n 50 -c classic -s large -t ${NAMEforANALYSIS} -o ${OUTPUT}_3end_length_of_piRNA_logo_prob.txt
#-------------------------------------------------------------------------------------------------------
#make compund 5end logo
#piRNA_start_pos total_counts cluster maxcol maxvalue
#chr2L!+@1000040 578 9 10 53.9792387543253
#generate a bedfile from the k-means clustering output add 3 nt around 5end
 cat ${FOLDER}_length-filtered.txt |  sed '1d' | grep -v "sep" | awk  -v NAME=${TMP} '  {OFS="\t"; split($1,a,"!"); split(a[2],b,"@"); if(b[1]=="+") print a[1],b[2],b[2]+3,$1,$2,b[1] > NAME"_5end_of_piRNA_plus.bed"; else print a[1],b[2]+1-3,b[2]+1,$1,$2,b[1] > NAME"_5end_of_piRNA_minus.bed" }'

#extract sequences from positions defined - get fasta was used to extract sequqnces
 cat ${TMP}_5end_of_piRNA_plus.bed | sort -k1,1 -k2,2n | bedtools getfasta -fi /groups/brennecke/mohn/indexes/dm3_r5_45/dmel-all-chromosome-r5.45.modif.for.index.wo.uextra.fasta -bed stdin -fo ${TMP}_5end_of_piRNA_plus.fasta
 #revers complement for minus strand is generated to generate a compound sequence logo 
 cat ${TMP}_5end_of_piRNA_minus.bed | sort -k1,1 -k2,2n | bedtools getfasta -fi /groups/brennecke/mohn/indexes/dm3_r5_45/dmel-all-chromosome-r5.45.modif.for.index.wo.uextra.fasta -bed stdin -fo ${TMP}_5end_of_piRNA_minus.fasta
 fastx_reverse_complement -i ${TMP}_5end_of_piRNA_minus.fasta -o ${TMP}_5end_of_piRNA_minus_rc.fasta
 cat ${TMP}_5end_of_piRNA_plus.fasta ${TMP}_5end_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U bits -A rna -F pdf -n 50 -c classic -s large -t ${NAMEforANALYSIS} -o ${OUTPUT}_5end_of_piRNA_logo.pdf
 cat ${TMP}_5end_of_piRNA_plus.fasta ${TMP}_5end_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U probability -A rna -F pdf -n 50 -c classic -o ${OUTPUT}_5end_of_piRNA_logo_prob.pdf
 cat ${TMP}_5end_of_piRNA_plus.fasta ${TMP}_5end_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U probability -A rna -F logodata -n 50 -c classic -s large -t ${NAMEforANALYSIS} -o ${OUTPUT}_5end_of_piRNA_logo_prob.txt
#-------------------------------------------------------------------------------------------------------
##make compund logo for position 10
##piRNA_start_pos total_counts cluster maxcol maxvalue
##chr2L!+@1000040 578 9 10 53.9792387543253
#generate a bedfile from the length clustering output add 3 nt around 5end
 cat ${FOLDER}_length-filtered.txt |  sed '1d' | grep -v "sep" | awk  -v NAME=${TMP} '  {OFS="\t"; split($1,a,"!"); split(a[2],b,"@"); if(b[1]=="+") print a[1],b[2]+8,b[2]+11,$1,$2,b[1] > NAME"_pos10_of_piRNA_plus.bed"; else print a[1],b[2]+1-11,b[2]+1-8,$1,$2,b[1] > NAME"_pos10_of_piRNA_minus.bed" }'

#extract sequences from positions defined - get fasta was used to extract sequqnces
 cat ${TMP}_pos10_of_piRNA_plus.bed | sort -k1,1 -k2,2n | bedtools getfasta -fi /groups/brennecke/mohn/indexes/dm3_r5_45/dmel-all-chromosome-r5.45.modif.for.index.wo.uextra.fasta -bed stdin -fo ${TMP}_pos10_of_piRNA_plus.fasta
#revers complement for minus strand is generated to generate a compound sequence logo 
 cat ${TMP}_pos10_of_piRNA_minus.bed | sort -k1,1 -k2,2n | bedtools getfasta -fi /groups/brennecke/mohn/indexes/dm3_r5_45/dmel-all-chromosome-r5.45.modif.for.index.wo.uextra.fasta -bed stdin -fo ${TMP}_pos10_of_piRNA_minus.fasta
 fastx_reverse_complement -i ${TMP}_pos10_of_piRNA_minus.fasta -o ${TMP}_pos10_of_piRNA_minus_rc.fasta
 cat ${TMP}_pos10_of_piRNA_plus.fasta ${TMP}_pos10_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U bits -A rna -F pdf -n 50 -c classic -s large -t ${NAMEforANALYSIS} -o ${OUTPUT}_pos10_of_piRNA_logo.pdf
 cat ${TMP}_pos10_of_piRNA_plus.fasta ${TMP}_pos10_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U probability -A rna -F pdf -n 50 -c classic -o ${OUTPUT}_pos10_of_piRNA_logo_prob.pdf
 cat ${TMP}_pos10_of_piRNA_plus.fasta ${TMP}_pos10_of_piRNA_minus_rc.fasta | tr 'T' 'U'| weblogo -U probability -A rna -F logodata -n 50 -c classic -s large -t ${NAMEforANALYSIS} -o ${OUTPUT}_pos10_of_piRNA_logo_prob.txt
#-------------------------------------------------------------------------------------------------------
#extract next nucleotide signatures at the 3end for 10bins starting at the most defined 3end going to the least defined 3ends using equally sized bins.

#store number in a variable ( sed '1d' deletes head) 
number=$(cat ${FOLDER}_length-filtered.txt | sed '1d' | wc -l)
number=$((number/10))

#sort according to definition and split into 10 equally sized bins from a - highest defined to j - lowest defined
cat ${FOLDER}_length-filtered.txt| sed '1d' | sort -n -k4,4 -r > ${FOLDER}_length-filtered_sorted.txt

split -l ${number} ${FOLDER}_length-filtered_sorted.txt ${TMP}_defined

for i in a b c d e f g h i j
do
#extract the next nucleotide for every bin and write it into a new file
  cat "${TMP}_defineda${i}" | grep -v "sep" | awk  -v NAME="${TMP}_${i}" '{OFS="\t"; split($1,a,"!"); split(a[2],b,"@"); if(b[1]=="+") print a[1],b[2]+$3,b[2]+$3+1,$1,$2,b[1] > NAME"_next_nt.bed"; else print a[1],b[2]+1-($3)-1,b[2]+1-($3),$1,$2,b[1] >> NAME"_next_nt.bed" }'
  cat "${TMP}_${i}_next_nt.bed" | sort -k1,1 -k2,2n | bedtools getfasta -s -fi /groups/brennecke/mohn/indexes/dm3_r5_45/dmel-all-chromosome-r5.45.modif.for.index.wo.uextra.fasta -bed stdin -fo "${TMP}_${i}_next_nt.fasta"

#extract number for every bin to calculate the average definiton per bin
  number=`cat ${TMP}_defineda${i} | wc -l `
  cat "${TMP}_defineda${i}" | grep -v "sep" | awk '{sum+=$4} END {print sum}' | awk -v NUMBER=$number -v I=$i -v NAME=${OUTPUT} '{OFS="\t"; print I,$1/NUMBER >> NAME"_average_def.txt"}'

  #extract downstream nucleotide counts per bin
  cat "${TMP}_${i}_next_nt.fasta" | awk '{if ($1 !~ ">") print }' | sort | uniq -c | awk -v NAME="${OUTPUT}_${i}" '{IFS=" "; OFS="\t"; print $2,$1 > NAME"_nucleotide_counts.txt" }'

done



